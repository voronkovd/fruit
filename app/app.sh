#!/bin/bash
cd ..
source env/bin/activate
cd app
exec gunicorn app.wsgi:application --workers 4 --bind 127.0.0.1:"$1"
