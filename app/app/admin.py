# -*- coding: utf-8 -*-
from django.contrib import admin
from image_cropping import ImageCroppingMixin
from suit.admin import SortableModelAdmin

from models import Page
from models import CategoryProduct
from models import Product
from models import PriceWeightProduct
from models import GalleryProduct
from forms import PageForm
from forms import ProductForm


class PageAdmin(SortableModelAdmin, admin.ModelAdmin):
    form = PageForm
    list_display = ('title', 'status')
    list_filter = ('title', 'status')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = True
    show_save_as_new = True
    show_delete_link = True
    show_save_and_add_another = True
    show_save_and_continue = True


class CategoryProductAdmin(SortableModelAdmin, admin.ModelAdmin):
    list_display = ('name', 'status')
    list_filter = ('name', 'status')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = True
    show_save_as_new = True
    show_delete_link = True
    show_save_and_add_another = True
    show_save_and_continue = True


class GalleryProductInline(admin.StackedInline):
    model = GalleryProduct
    extra = 0


class PriceWeightProductInline(admin.StackedInline):
    model = PriceWeightProduct
    extra = 0


class ProductAdmin(ImageCroppingMixin, SortableModelAdmin, admin.ModelAdmin):
    form = ProductForm
    list_display = ('name', 'image_tag', 'status')
    list_filter = ('name', 'status')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = True
    show_save_as_new = True
    show_delete_link = True
    show_save_and_add_another = True
    show_save_and_continue = True
    inlines = (GalleryProductInline, PriceWeightProductInline)


class RelationWeightProductAdmin(admin.ModelAdmin):
    list_display = ('product', 'weight', 'price')
    list_filter = ('product__name', 'weight', 'price')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = True
    show_save_as_new = True
    show_delete_link = True
    show_save_and_add_another = True
    show_save_and_continue = True


admin.site.register(Page, PageAdmin)
admin.site.register(CategoryProduct, CategoryProductAdmin)
admin.site.register(Product, ProductAdmin)
