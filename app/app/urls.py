# -*- coding: utf-8 -*-
from django.conf.urls import patterns
from django.conf.urls import include
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.contrib.sitemaps.views import sitemap as sm
from django.contrib.sitemaps import GenericSitemap

from views import index
from views import product as prod
from views import page
from sitemap import product_dict
from sitemap import page_dict


admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', index, name='index'),
                       url(r'^product/(?P<pk>\d+)/$', prod, name='product'),
                       url(r'^page/(?P<slug>\w+)/$', page, name='page'),
                       url(r'^sitemap\.xml$',
                           sm,
                           {'sitemaps':
                            {'products':
                                GenericSitemap(product_dict, priority=0.9),
                             'pages':
                                 GenericSitemap(page_dict, priority=0.6)}},
                           name='django.contrib.sitemaps.views.sitemap'),
                       url(r'^admin/', include(admin.site.urls)),
                       (r'^ckeditor/', include('ckeditor.urls')),
                       (r'^media/(?P<path>.*)$', 'django.views.static.serve',
                        {'document_root': settings.MEDIA_ROOT}),
                       (r'^static/(?P<path>.*)$', 'django.views.static.serve',
                        {'document_root': settings.STATIC_ROOT}))
