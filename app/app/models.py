# -*- coding: utf-8 -*
import os
from uuid import uuid4

from django.conf import settings
from django.db import models
from easy_thumbnails.files import get_thumbnailer
from image_cropping import ImageRatioField


def path_and_rename(path):
    def wrapper(instance, filename):
        ext = filename.split('.')[-1]
        if instance.pk:
            filename = '{}.{}'.format(instance.pk, ext)
        else:
            filename = '{}.{}'.format(uuid4().hex, ext)
        return os.path.join(path, filename)

    return wrapper


class Page(models.Model):
    slug = models.CharField(verbose_name=u'Ссылка', max_length=128,
                            unique=True, db_index=True)
    title = models.CharField(verbose_name=u'Заголовок',
                             max_length=128, unique=True)
    content = models.TextField(verbose_name=u'Содержимое')
    status = models.BooleanField(verbose_name=u'Статус', default=False)
    order = models.PositiveIntegerField(verbose_name=u'Сортировка')

    class Meta:
        app_label = 'app'
        db_table = 'pages'
        verbose_name = u'Страницу'
        verbose_name_plural = u'Страницы'

    def __unicode__(self):
        return self.title


class CategoryProduct(models.Model):
    name = models.CharField(max_length=32, verbose_name=u'Наименованание')
    order = models.PositiveIntegerField(verbose_name=u'Сортировка')
    status = models.BooleanField(default=False, verbose_name=u'Статус')

    class Meta:
        app_label = 'app'
        db_table = 'category_products'
        verbose_name = u'Категорию товара'
        verbose_name_plural = u'Категории товара'

    def __unicode__(self):
        return self.name


class Product(models.Model):
    category = models.ForeignKey(verbose_name=u'Категория', to=CategoryProduct)
    name = models.CharField(verbose_name=u'Наименование', max_length=64)
    description = models.TextField(verbose_name=u'Описание')
    image = models.ImageField(verbose_name=u'Изображение',
                              upload_to=path_and_rename('images/products/'))
    cropping = ImageRatioField(verbose_name=u'Пропорции изображения',
                               image_field='image', size='400x400')
    order = models.PositiveIntegerField(verbose_name=u'Сортировка', null=True)
    status = models.BooleanField(verbose_name=u'Опубликовать', default=False)

    class Meta:
        db_table = 'products'
        app_label = 'app'
        verbose_name = u'Товар'
        verbose_name_plural = u'Товары'

    def __unicode__(self):
        return self.name

    def image_tag(self):
        file_image = settings.MEDIA_ROOT + str(self.image.__unicode__())
        if self.image and os.path.isfile(file_image):
            options = {'size': (100, 100), 'crop': True, 'autocrop': True}
            thumb_url = get_thumbnailer(self.image).get_thumbnail(options).url
            return '<img src="' + thumb_url + '" />'

    image_tag.short_description = u'Изображение'

    image_tag.allow_tags = True

    def delete(self, *args, **kwargs):
        storage, path = self.image.storage, self.image.path
        super(Product, self).delete(*args, **kwargs)
        storage.delete(path)

    def get_absolute_url(self):
        return '/product/%s' % str(self.pk)


class PriceWeightProduct(models.Model):
    product = models.ForeignKey(verbose_name=u'Товар', to=Product,
                                related_name='product')
    weight = models.IntegerField(verbose_name=u'Вес', help_text=u'кг.')
    price = models.DecimalField(verbose_name=u'Цена', max_digits=7,
                                decimal_places=2, help_text=u'руб.')

    class Meta:
        db_table = 'price_weight_products'
        app_label = 'app'
        verbose_name = u'Цену и вес на товар'
        verbose_name_plural = u'Цены и вес на товары'

    def __unicode__(self):
        return self.product.name


class GalleryProduct(models.Model):
    product = models.ForeignKey(verbose_name=u'Товар', to=Product)
    image = models.ImageField(verbose_name=u'Изображение',
                              upload_to=path_and_rename('images/gallery/'))

    class Meta:
        db_table = 'gallery_products'
        app_label = 'app'
        verbose_name = u'Изображение товара'
        verbose_name_plural = u'Изображения товаров'

    def __unicode__(self):
        return self.product.name

    def image_tag(self):
        file_image = settings.MEDIA_ROOT + str(self.image.__unicode__())
        if self.image and os.path.isfile(file_image):
            options = {'size': (100, 100), 'crop': True, 'autocrop': True}
            thumb_url = get_thumbnailer(self.image).get_thumbnail(options).url
            return '<img src="' + thumb_url + '" />'

    image_tag.short_description = u'Изображение'

    image_tag.allow_tags = True

    def delete(self, *args, **kwargs):
        storage, path = self.image.storage, self.image.path
        super(GalleryProduct, self).delete(*args, **kwargs)
        storage.delete(path)
