# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from django.views.generic import DetailView
from django.views.generic import ListView

from models import Product
from models import Page
from models import CategoryProduct


class ProductListMixin(object):
    def get_products(self):
        return Product.objects.filter(status=True).order_by('order').all()

    def get_categories(self):
        return CategoryProduct.objects.filter(
            status=True).order_by('order').all()

    def get_context_data(self, **kwargs):
        ctx = super(ProductListMixin, self).get_context_data(**kwargs)
        ctx['categories'] = self.get_categories()
        ctx['products'] = self.get_products()
        return ctx


class IndexView(ProductListMixin, ListView):
    model = Product
    template_name = 'product_list.html'


class ProductView(DetailView):
    model = Product
    template_name = 'product_detail.html'


class PageView(DetailView):
    model = Page
    template_name = 'page.html'


index = IndexView.as_view()
product = ProductView.as_view()
page = PageView.as_view()
