# -*- coding: utf-8 -*-
import os
import ConfigParser

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
from easy_thumbnails.conf import Settings as thumbnail_settings

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

my_config = ConfigParser.ConfigParser()
my_config.readfp(open(BASE_DIR + '/app/settings.ini'))

SECRET_KEY = my_config.get('main', 'secret_key')

DEBUG = my_config.getboolean('main', 'debug')

TEMPLATE_DEBUG = DEBUG

ADMINS = (('Voronkov D.', 'mail@voronkovd.ru'),)

ALLOWED_HOSTS = ['*']

ROOT_URLCONF = 'app.urls'

WSGI_APPLICATION = 'app.wsgi.application'

LANGUAGE_CODE = 'ru-Ru'

TIME_ZONE = 'Asia/Novosibirsk'

USE_I18N = True

USE_L10N = True

USE_TZ = True

INSTALLED_APPS = (
    'easy_thumbnails',
    'image_cropping',
    'suitlocale',
    'ckeditor',
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'app',
)
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': my_config.get('mysql', 'name'),
        'USER': my_config.get('mysql', 'user'),
        'PASSWORD': my_config.get('mysql', 'password'),
        'HOST': my_config.get('mysql', 'host'),
    }
}
TEMPLATE_DIRS = (BASE_DIR + 'templates/',)

MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR + '/media/'

STATIC_ROOT = BASE_DIR + '/static/'
STATICFILES_DIRS = ()
STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_HOST_PASSWORD = my_config.get('email', 'password')
EMAIL_HOST_USER = my_config.get('email', 'user')
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
EMAIL_PORT = 465
EMAIL_USE_SSL = True

LOGGING_CONFIG = None

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',)

THUMBNAIL_PROCESSORS = ('image_cropping.thumbnail_processors.crop_corners',
                        ) + thumbnail_settings.THUMBNAIL_PROCESSORS

IMAGE_CROPPING_SIZE_WARNING = True

CKEDITOR_UPLOAD_PATH = "uploads/"

SUIT_CONFIG = {
    'ADMIN_NAME': u'Фруктовые букеты',
    'HEADER_DATE_FORMAT': False,
    'HEADER_TIME_FORMAT': 'H:i',
    'SHOW_REQUIRED_ASTERISK': True,
    'CONFIRM_UNSAVED_CHANGES': True,
    'SEARCH_URL': ''
}

SITE_NAME_TITLE = 'Фруктовые букеты'

CKEDITOR_CONFIGS = {
    'default': {
        'height': 300,
        'width': 600,
    },
}
