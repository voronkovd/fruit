# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0430\u043d\u0438\u0435')),
                ('order', models.PositiveIntegerField(verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430')),
                ('status', models.BooleanField(default=False, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441')),
            ],
            options={
                'db_table': 'category_products',
                'verbose_name': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044e',
                'verbose_name_plural': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='GalleryProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=None, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
            ],
            options={
                'db_table': 'gallery_products',
                'verbose_name': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u0430',
                'verbose_name_plural': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u0442\u043e\u0432\u0430\u0440\u043e\u0432',
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.CharField(unique=True, max_length=128, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', db_index=True)),
                ('title', models.CharField(unique=True, max_length=128, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('content', models.TextField(verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435')),
                ('status', models.BooleanField(default=False, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441')),
                ('order', models.PositiveIntegerField(verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430')),
            ],
            options={
                'db_table': 'pages',
                'verbose_name': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0443',
                'verbose_name_plural': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u044b',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('image', models.ImageField(upload_to=None, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                (b'cropping', image_cropping.fields.ImageRatioField(b'image', '400x400', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u041f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f')),
                ('order', models.PositiveIntegerField(null=True, verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430')),
                ('status', models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u0442\u044c')),
                ('category', models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='app.CategoryProduct')),
            ],
            options={
                'db_table': 'products',
                'verbose_name': '\u0422\u043e\u0432\u0430\u0440',
                'verbose_name_plural': '\u0422\u043e\u0432\u0430\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='RelationWeightProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430', max_digits=7, decimal_places=2, db_index=True)),
                ('product', models.ForeignKey(related_name='product', verbose_name='\u0422\u043e\u0432\u0430\u0440', to='app.Product')),
            ],
            options={
                'db_table': 'relation_weight_products',
                'verbose_name': '\u0426\u0435\u043d\u0443 \u043d\u0430 \u0442\u043e\u0432\u0430\u0440',
                'verbose_name_plural': '\u0426\u0435\u043d\u044b \u043d\u0430 \u0442\u043e\u0432\u0430\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='WeightProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=12, verbose_name='\u0412\u0435\u0441')),
            ],
            options={
                'db_table': 'weight_products',
                'verbose_name': '\u0412\u0430\u0440\u0438\u0430\u043d\u0442 \u0432\u0435\u0441\u0430',
                'verbose_name_plural': '\u0412\u0430\u0440\u0438\u0430\u043d\u0442\u044b \u0432\u0435\u0441\u0430',
            },
        ),
        migrations.AddField(
            model_name='relationweightproduct',
            name='weight',
            field=models.ForeignKey(related_name='weight', verbose_name='\u0412\u0435\u0441', to='app.WeightProduct'),
        ),
        migrations.AddField(
            model_name='galleryproduct',
            name='product',
            field=models.ForeignKey(verbose_name='\u0422\u043e\u0432\u0430\u0440', to='app.Product'),
        ),
    ]
