# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PriceWeightProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('weight', models.IntegerField(help_text='\u043a\u0433.', verbose_name='\u0412\u0435\u0441')),
                ('price', models.DecimalField(help_text='\u0440\u0443\u0431.', verbose_name='\u0426\u0435\u043d\u0430', max_digits=7, decimal_places=2)),
                ('product', models.ForeignKey(related_name='product', verbose_name='\u0422\u043e\u0432\u0430\u0440', to='app.Product')),
            ],
            options={
                'db_table': 'price_weight_products',
                'verbose_name': '\u0426\u0435\u043d\u0443 \u0438 \u0432\u0435\u0441 \u043d\u0430 \u0442\u043e\u0432\u0430\u0440',
                'verbose_name_plural': '\u0426\u0435\u043d\u044b \u0438 \u0432\u0435\u0441 \u043d\u0430 \u0442\u043e\u0432\u0430\u0440\u044b',
            },
        ),
        migrations.RemoveField(
            model_name='relationweightproduct',
            name='product',
        ),
        migrations.RemoveField(
            model_name='relationweightproduct',
            name='weight',
        ),
        migrations.DeleteModel(
            name='RelationWeightProduct',
        ),
        migrations.DeleteModel(
            name='WeightProduct',
        ),
    ]
