# -*- coding: utf-8 -*-
from django.apps import AppConfig


class CarvingConfig(AppConfig):
    name = 'app'
    verbose_name = u'Управление сайтом'