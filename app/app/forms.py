# -*- coding: utf-8 -*-
from django.forms import ModelForm
from ckeditor.widgets import CKEditorWidget
from suit.widgets import LinkedSelect


class PageForm(ModelForm):
    class Meta:
        widgets = {'content': CKEditorWidget}


class ProductForm(ModelForm):
    class Meta:
        widgets = {'description': CKEditorWidget}


class RelationProductWeightForm(ModelForm):
    class Meta:
        widgets = {'product': LinkedSelect()}
