# -*- coding: utf-8 -*-
from models import Product
from models import Page

product_dict = {'queryset': Product.objects.filter(status=True).all()}
page_dict = {'queryset': Page.objects.filter(status=True).all()}
