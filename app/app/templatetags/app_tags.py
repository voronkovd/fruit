# coding: utf-8
from django import template
from app.models import Page
from django.conf import settings


register = template.Library()


@register.inclusion_tag('tags/__menu.html')
def menu_link():
    pages = Page.objects.filter(status=True).order_by('order').all()
    return {'pages': pages}


@register.simple_tag
def site_name():
    return settings.SITE_NAME_TITLE
